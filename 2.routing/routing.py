from flask import Flask, request

app = Flask(__name__)

# matching the root
@app.route('/')
def index():
    return 'Index Page'

# matching specific URL
@app.route('/hello')
def hello():
    return 'Hello, World'

# using URL argument ?id=...
@app.route('/product')
def show_product():
    return 'Product id %s' % request.args.get('id', 'default')

# using URL path variable
@app.route('/user/<username>')
def show_user_profile(username):
    return 'User %s' % username
