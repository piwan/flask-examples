from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    links = [
        { 'href': 'https://google.com', 'caption': 'Google'},
        { 'href': 'https://www.yahoo.com', 'caption': 'Yahoo'},
        { 'href': 'http://bbc.com', 'caption': 'BBC'},
        { 'href': 'https://cnn.com', 'caption': 'CNN'}
    ]
    return render_template('hello.html', name=name, navigation=links)
