from flask import Flask, request, redirect, url_for

app = Flask(__name__)


@app.route('/login', methods=['GET'])
def login_form():
    message = request.args.get('msg', '')
    return '''
        <h1>You need to login</h1>
        <form action="/login" method="post">
            Login: <input name="login" /><br />
            Passw: <input name="passw" /><br />
            <input type="submit" value="Submit"/>
        </form>
        <p style="color:red">%s</p>
    ''' % message


@app.route('/login', methods=['POST'])
def login():
    try:
        if request.form['passw'] == 'letmein2':
            return '''
                <h1>Hello, %s</h1>
            ''' % request.form['login']
        else:
            return redirect(url_for('login_form', msg='wrong password!'))
    except NameError:
        return login_form()
