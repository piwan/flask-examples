from flask import Flask

# init flask with package name (we have only one - let's use main)
app = Flask(__name__)

# decorator to install URL
@app.route("/")
def hello():
    return "Hello World!"
